File Upload Demo
================

Minimal Ruby on Rails 7 application for a commonplace application stack:

- PostgreSQL
- Redis
- Ruby 3.1.2p20
- Node.js v18.11.0
- esbuild
- Tailwind
- Devise

### License

- [WTFPL](http://www.wtfpl.net)
